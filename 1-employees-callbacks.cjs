const path = require('path')
const fs = require('fs')

const jsondata = path.join(__dirname, './data.json')
const idsDataPath = path.join(__dirname, './idsdata.json')
const groupedDataPath = path.join(__dirname, './groupedData.json')
const PowerpuffBrigadeDataPath = path.join(__dirname, './powerpuffbrigade.json')
const removedIdDataPath = path.join(__dirname, './removedId')
/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

function getDataOfRemovedId(employeesData, id) {
    let removedIdData = JSON.parse(employeesData).employees
        .filter(eachData => {
            return eachData.id !== id
        })
    return JSON.stringify(removedIdData)

}

function getPowerPuffBrigadeData(employeesData) {
    let companyData = JSON.parse(employeesData).employees
        .filter(eachItem => {
            return eachItem.company == "Powerpuff Brigade"
        })
    return JSON.stringify(companyData)


}

function group(employeesData) {

    let groupData = JSON.parse(employeesData).employees
        .reduce((resObj, eachData) => {
            if (eachData.company == 'Scooby Doo') {
                resObj['Scooby Doo'].push(eachData)

            } else if (eachData.company == 'Powerpuff Brigade') {
                resObj['Powerpuff Brigade'].push(eachData)

            } else if (eachData.company == 'X-Men') {
                resObj['X-Men'].push(eachData)
            }


            return resObj
        }, {
            'Scooby Doo': [],
            "Powerpuff Brigade": [],
            "X-Men": []
        })
    return JSON.stringify(groupData)



}

function retriveDataOfId(employeesData) {
    let ids = [2, 13, 23]
    let stringifiedData = JSON.parse(employeesData)
    let idsData = stringifiedData.employees
        .reduce((resObj, eachData) => {
            if (eachData.id == 2 || eachData.id == 13 || eachData.id == 23) {
                if (resObj['employees']) {
                    resObj['employees'].push(eachData)
                } else {
                    resObj['employees'] = []
                    resObj['employees'].push(eachData)
                }
            }

            return resObj
        }, {})

    return JSON.stringify(idsData)


}

function problem(callback) {
    fs.readFile(path.join(__dirname, './data.json'), (err, employeesData) => {
        if (err) {
            console.error(err)
            return callback(err)

        } else {
            callback(null, "Reading Successfull")
            let idsData = retriveDataOfId(employeesData.toString())

            fs.writeFile(idsDataPath, idsData, (err) => {
                if (err) {
                    console.error(err)
                    return callback(err)
                } else {
                    callback(null, 'Success idsdata')
                    let groupedData = group(employeesData)
                    fs.writeFile(groupedDataPath, groupedData, (err) => {
                        if (err) {
                            console.err(err)
                            return callback(err)
                        } else {
                            callback(null, "Success grouping")
                            let powerpuffBrigadeData = getPowerPuffBrigadeData(employeesData)
                            fs.writeFile(PowerpuffBrigadeDataPath, powerpuffBrigadeData, (err) => {
                                if (err) {
                                    console.error(err)
                                    return callback(err)
                                } else {
                                    callback(null, 'Success Powerpuff Brigade Company Data')
                                    let idRemovedData = getDataOfRemovedId(employeesData, 2)
                                    fs.writeFile(removedIdDataPath, idRemovedData, (err) => {
                                        if (err) {
                                            console.error(err)
                                            callback(err)
                                        } else {
                                            callback(null, "Success")
                                        }
                                    })

                                }
                            })
                        }
                    })
                }
            })


        }
    })

}


problem((err, data) => {
    if (err) {
        console.error(err)
    } else {
        console.log(data)
    }

})